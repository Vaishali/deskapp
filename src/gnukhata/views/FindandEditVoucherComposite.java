package gnukhata.views;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import gnukhata.globals;
import gnukhata.controllers.StartupController;
import gnukhata.controllers.transactionController;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

import sun.misc.Cleaner;


public class FindandEditVoucherComposite extends Composite 
{	
	static Display display;
	public static String typeFlag;
	int counter=0;
	Label lblVoucherNo;
	Label lblNarration;
	Text txtnarration;
	Button save;
	Label lblsearchRec;
	Combo combosearchRec;
	Label lblEntVoucherNo;
	Text txtEntVoucherNo;
	Text txtFromddate;
	Label lblFromDtDash1;
	Label lblNarrationHeader;
	Label lblFromDate;
	Text txtFromMdate;
	Label lblFromDtDash2;
	Text txtFromYrdate;
	Label lblToDate;
	Text txtToDdate;
	Label lblToDateDash1;
	Text txtToMdate;
	Label lblToDateDash2;
	Text txtToYrdate;
	Label lblentamount;
	Text txtentamount;
	Button btndelete;
	Button btnsearch;
	Table tableVoucherRecord;
	Label lblVoucherType;
	Label lblDateOfTtrans;
	Label lblDrAccount;
	Label lblCrAccount;
	Label lblAmount;
	Label lblProjectName;
	Label lblaction;
	Label lblMarkForDel;
	int totalWidth = 0;
	
	Label lblvoucherno;
	Label lblvoucherdate;
	Label lblvouchertype;
	Label lbldraccount;
	Label lblcraccount;
	Label lblamount;
	Label lblprojectname;
	Label lblnarration;
	boolean verifyFlag=false;
	
	ArrayList<Label> voucherNo=new ArrayList<Label>();
	ArrayList<Label> voucherType=new ArrayList<Label>();
	ArrayList<Label> voucherDate=new ArrayList<Label>();
	ArrayList<Label> voucherDrAccount=new ArrayList<Label>();
	ArrayList<Label> voucherCrAccount=new ArrayList<Label>();
	ArrayList<Label> voucherAmount=new ArrayList<Label>();
	ArrayList<Label> voucherProjectName=new ArrayList<Label>();
	ArrayList<Label> voucherNarration=new ArrayList<Label>();
	ArrayList<Button> viewButton=new ArrayList<Button>();
	ArrayList<Button> ChkDelButton=new ArrayList<Button>();
	
	Object[] projectlist;
	Group grpVoucherResult;
	ScrolledComposite scVoucherResult;
	Composite innerComp;
	NumberFormat nf;
	boolean findvoucher;

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	Vector<Button> forDelete = new Vector<Button>();
	Vector<Button> forView = new Vector<Button>();
	
	public FindandEditVoucherComposite(Composite parent, int style, boolean findvoucherflag) {
		
		super(parent, style);
		
		findvoucher=findvoucherflag;
		
		Date today = new Date();
		String strToday = sdf.format(today);
		nf = NumberFormat.getInstance();
		nf.setGroupingUsed(false);
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(2);
		FormLayout formlayout = new FormLayout();
		this.setLayout(formlayout);
		// Find/Edit/delete Records
		lblsearchRec = new Label(this,SWT.NONE);
		lblsearchRec.setText("S&earch Record By : ");
		lblsearchRec.setFont(new Font(display, "Time New Roman",10,SWT.NORMAL));
		FormData layout = new FormData();
		layout.top = new FormAttachment(5);
		layout.left = new FormAttachment(1);
		layout.right = new FormAttachment(12);
		layout.bottom = new FormAttachment(11);
		lblsearchRec.setLayoutData(layout);
		
		combosearchRec = new Combo(this,SWT.READ_ONLY );
		combosearchRec.setFont(new Font(display,"Times New Romen",9,SWT.NONE));
		combosearchRec.add("           ---- Please Select -----        ");
		combosearchRec.add("Voucher No");
		combosearchRec.add("Time Interval (From-To)");
		combosearchRec.add("Amount");
		combosearchRec.add("Narration");
		combosearchRec.select(0);
		layout = new FormData();
		layout.top = new FormAttachment(5);
		layout.left = new FormAttachment(13);
		layout.right = new FormAttachment(30);
		layout.bottom = new FormAttachment(11);
		combosearchRec.setLayoutData(layout);
//search voucher by voucher number		
		
		lblEntVoucherNo = new Label(this,SWT.NONE);
		lblEntVoucherNo.setText("Enter &Voucher No : ");
		lblEntVoucherNo.setFont(new Font(display, "Time New Roman",10,SWT.NORMAL));
		layout = new FormData();
		layout.top = new FormAttachment(5);
		layout.left = new FormAttachment(39);
		layout.right = new FormAttachment(53);
		layout.bottom = new FormAttachment(11);
		lblEntVoucherNo.setLayoutData(layout);
		lblEntVoucherNo.setVisible(false);
		
		txtEntVoucherNo = new Text(this,SWT.BORDER);
		txtEntVoucherNo.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(5);
		layout.left = new FormAttachment(54);
		layout.right = new FormAttachment(64);
		layout.bottom = new FormAttachment(11);
		txtEntVoucherNo.setLayoutData(layout);
		txtEntVoucherNo.setVisible(false);
		
		
		
		//date fields when search voucher by time interval(From-to)
		lblFromDate = new Label(this,SWT.NONE);
		lblFromDate.setText("From &Date: ");
		lblFromDate.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
		layout = new FormData();
		layout.top = new FormAttachment(5);
		layout.left = new FormAttachment(37);
		layout.right = new FormAttachment(45);
		layout.bottom = new FormAttachment(11);
		lblFromDate.setLayoutData(layout);
		lblFromDate.setVisible(false);
		
		txtFromddate = new Text(this,SWT.BORDER);
		txtFromddate.setFont(new Font(display,"Times New Romen",7,SWT.NONE));
		txtFromddate.setTextLimit(2);
		txtFromddate.setMessage("dd");
		txtFromddate.setText(globals.session[2].toString().substring(0,2));
		layout = new FormData();
		layout.top = new FormAttachment(5);
		layout.left = new FormAttachment(46);
		layout.right = new FormAttachment(49);
		layout.bottom = new FormAttachment(9);
		txtFromddate.selectAll();
		txtFromddate.setLayoutData(layout);
		txtFromddate.setVisible(false);
		
		lblFromDtDash1 = new Label(this, SWT.NONE);
		lblFromDtDash1.setText("-");
		lblFromDtDash1.setFont(new Font(display, "Time New Roman",12,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(5);
		layout.left = new FormAttachment(49);
		layout.right = new FormAttachment(50);
		layout.bottom = new FormAttachment(9);
		lblFromDtDash1.setLayoutData(layout);
		lblFromDtDash1.setVisible(false);
		
		txtFromMdate = new Text(this,SWT.BORDER);
		txtFromMdate.setFont(new Font(display,"Times New Romen",7,SWT.NONE));
		txtFromMdate.setTextLimit(2);
		txtFromMdate.setMessage("mm");
		txtFromMdate.setText(globals.session[2].toString().substring(3,5));
		layout = new FormData();
		layout.top = new FormAttachment(5);
		layout.left = new FormAttachment(50);
		layout.right = new FormAttachment(53);
		layout.bottom = new FormAttachment(9);
		txtFromMdate.selectAll();
		txtFromMdate.setLayoutData(layout);		
		txtFromMdate.setVisible(false);
		
		lblFromDtDash2 = new Label(this, SWT.NONE);
		lblFromDtDash2.setText("-");
		lblFromDtDash2.setFont(new Font(display, "Time New Roman",12,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(5);
		layout.left = new FormAttachment(53);
		layout.right = new FormAttachment(54);
		layout.bottom = new FormAttachment(9);
		lblFromDtDash2.setLayoutData(layout);
		lblFromDtDash2.setVisible(false);
		
		txtFromYrdate = new Text(this,SWT.BORDER);
		txtFromYrdate.setFont(new Font(display,"Times New Romen",7,SWT.NONE));
		txtFromYrdate.setTextLimit(4);
		txtFromYrdate.setMessage("yyyy");
		txtFromYrdate.setText(globals.session[2].toString().substring(6));
		layout = new FormData();
		layout.top = new FormAttachment(5);
		layout.left = new FormAttachment(54);
		layout.right = new FormAttachment(59);
		layout.bottom = new FormAttachment(9);
		txtFromYrdate.setLayoutData(layout);
		txtFromYrdate.selectAll();
		txtFromYrdate.setVisible(false);
		
		lblToDate = new Label(this,SWT.NONE);
		lblToDate.setText("T&o Date :");
		lblToDate.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
		layout = new FormData();
		layout.top = new FormAttachment(12);
		layout.left = new FormAttachment(37);
		layout.right = new FormAttachment(45);
		layout.bottom = new FormAttachment(18);
		lblToDate.setLayoutData(layout);
		lblToDate.setVisible(false);
		
		txtToDdate = new Text(this,SWT.BORDER);
		txtToDdate.setFont(new Font(display,"Times New Romen",7,SWT.NONE));
		txtToDdate.setTextLimit(2);
		txtToDdate.setMessage("dd");
		txtToDdate.setText(globals.session[3].toString().substring(0,2));
		layout = new FormData();
		layout.top = new FormAttachment(12);
		layout.left = new FormAttachment(46);
		layout.right = new FormAttachment(49);
		layout.bottom = new FormAttachment(16);
		txtToDdate.selectAll();
		txtToDdate.setLayoutData(layout);
		txtToDdate.setVisible(false);
		
		lblToDateDash1 = new Label(this, SWT.NONE);
		lblToDateDash1.setText("-");
		lblToDateDash1.setFont(new Font(display, "Time New Roman",12,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(12);
		layout.left = new FormAttachment(49);
		layout.right = new FormAttachment(50);
		layout.bottom = new FormAttachment(16);
		lblToDateDash1.setLayoutData(layout);
		lblToDateDash1.setVisible(false);
		
		txtToMdate = new Text(this,SWT.BORDER);
		txtToMdate.setFont(new Font(display,"Times New Romen",7,SWT.NONE));
		txtToMdate.setTextLimit(2);
		txtToMdate.setMessage("mm");
		txtToMdate.setText(globals.session[3].toString().substring(3,5));
		layout = new FormData();
		layout.top = new FormAttachment(12);
		layout.left = new FormAttachment(50);
		layout.right = new FormAttachment(53);
		layout.bottom = new FormAttachment(16);
		txtToMdate.selectAll();
		txtToMdate.setLayoutData(layout);
		txtToMdate.setVisible(false);
		
		lblToDateDash2 = new Label(this, SWT.NONE);
		lblToDateDash2.setText("-");
		lblToDateDash2.setFont(new Font(display, "Time New Roman",12,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(12);
		layout.left = new FormAttachment(53);
		layout.right = new FormAttachment(54);
		layout.bottom = new FormAttachment(16);
		lblToDateDash2.setLayoutData(layout);
		lblToDateDash2.setVisible(false);
		
		txtToYrdate = new Text(this,SWT.BORDER);
		txtToYrdate.setFont(new Font(display,"Times New Romen",7,SWT.NONE));
		txtToYrdate.setTextLimit(4);
		txtToYrdate.setMessage("yyyy");
		txtToYrdate.setText(globals.session[3].toString().substring(6));
		layout = new FormData();
		layout.top = new FormAttachment(12);
		layout.left = new FormAttachment(54);
		layout.right = new FormAttachment(59);
		layout.bottom = new FormAttachment(16);
		txtToYrdate.selectAll();
		txtToYrdate.setLayoutData(layout);
		txtToYrdate.setVisible(false);
		
// searching records by entering  Amount 
		lblentamount = new Label(this, SWT.NONE);
		lblentamount.setText("Enter Amo&unt :");
		lblentamount.setFont(new Font(display, "Time New Roman",10,SWT.NORMAL));
		layout = new FormData();
		layout.top = new FormAttachment(5);
		layout.left = new FormAttachment(37);
		layout.right = new FormAttachment(51);
		layout.bottom = new FormAttachment(11);
		lblentamount.setLayoutData(layout);
		lblentamount.setVisible(false);
		
		txtentamount = new Text(this,SWT.RIGHT | SWT.BORDER);
		txtentamount.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		txtentamount.setMessage("0.00");
		layout = new FormData();
		layout.top = new FormAttachment(5);
		layout.left = new FormAttachment(52);
		layout.right = new FormAttachment(63);
		layout.bottom = new FormAttachment(11);
		txtentamount.selectAll();
		txtentamount.setLayoutData(layout);
		txtentamount.setVisible(false);
		
// searching records by narration
		lblNarration = new Label(this, SWT.NONE);
		lblNarration.setText("Enter Narrat&ion containing :");
		lblNarration.setFont(new Font(display, "Time New Roman",10,SWT.None));
		layout = new FormData();
		layout.top = new FormAttachment(5);
		layout.left = new FormAttachment(37);
		layout.right = new FormAttachment(53);
		layout.bottom = new FormAttachment(11);
		lblNarration.setLayoutData(layout);
		lblNarration.setVisible(false);
		
		txtnarration = new Text(this,SWT.BORDER | SWT.MULTI | SWT.WRAP);
		layout = new FormData();
		layout.top = new FormAttachment(5);
		layout.left = new FormAttachment(56);
		layout.right = new FormAttachment(80);
		layout.bottom = new FormAttachment(18);
		txtnarration.setLayoutData(layout);
		txtnarration.setVisible(false);
		
		btnsearch = new Button(this,SWT.PUSH );
		btnsearch.setText("&Search");
		btnsearch.setFont(new Font(display,"Time New Roman",10,SWT.None ));
		layout = new FormData();
		layout.top= new FormAttachment(5);
		layout.left= new FormAttachment(85);
		layout.right= new FormAttachment(92);
		layout.bottom= new FormAttachment(12);
		btnsearch.setLayoutData(layout);
		btnsearch.setEnabled(false);
		
		
		projectlist=transactionController.getAllProjects();
		
		grpVoucherResult=new Group(this, SWT.BORDER | SWT.V_SCROLL);
		grpVoucherResult.setLayout(new GridLayout(1, false));
		layout=new FormData();
		layout.top = new FormAttachment(btnsearch,32);
		layout.left = new FormAttachment(1);
		layout.right = new FormAttachment(grpVoucherResult,5);
		layout.bottom = new FormAttachment(90);
		grpVoucherResult.setLayoutData(layout);
		GridLayout gl = new GridLayout();
		grpVoucherResult.setVisible(false);
		gl.numColumns=10;
		grpVoucherResult.setBounds(this.getDisplay().getPrimaryMonitor().getBounds());
		grpVoucherResult.setLayout(gl);
		
			
		scVoucherResult = new ScrolledComposite(grpVoucherResult, SWT.V_SCROLL | SWT.H_SCROLL);
		scVoucherResult.setLayout(new GridLayout(1,false));
		GridData gdd=new GridData(SWT.FILL, SWT.FILL, true, true);
		gdd.heightHint=300;
	//	gdd.widthHint=1100;
		scVoucherResult.setBounds(this.getDisplay().getPrimaryMonitor().getBounds());
		scVoucherResult.setLayoutData(gdd);
		    
		innerComp = new Composite(scVoucherResult, SWT.BORDER);
		gl = new GridLayout(1,false);
		gl.numColumns=10;
		innerComp.setLayout(gl);
		gdd = new GridData(SWT.FILL, SWT.FILL, true, true);
		//gdd.heightHint=340;
		//gdd.widthHint=1100;
		innerComp.setBounds(this.getDisplay().getPrimaryMonitor().getBounds());
		innerComp.setLayoutData(gdd);
		
		lblVoucherNo = new Label(innerComp,SWT.BORDER);
		lblVoucherNo.setText("    Voucher \n      No");
		GridData gd=new GridData();
		lblVoucherNo.setLayoutData(gd);
		//gd.widthHint=75;
		
		lblVoucherType = new Label(innerComp,SWT.BORDER);
		lblVoucherType.setText("   Voucher \n   Type");
		gd=new GridData();
		//gd.widthHint=90;
		lblVoucherType.setLayoutData(gd);
		
		lblDateOfTtrans= new Label(innerComp,SWT.BORDER);
		lblDateOfTtrans.setText("Date Of \n Transaction");
		gd=new GridData();
		//gd.widthHint=103;
		lblVoucherType.setLayoutData(gd);
		
		lblDrAccount = new Label(innerComp,SWT.BORDER);
		lblDrAccount.setText("       Dr \n     Account");
		gd=new GridData();
		//gd.widthHint=95;
		lblDrAccount.setLayoutData(gd);
		
		lblCrAccount = new Label(innerComp,SWT.BORDER);
		lblCrAccount.setText("       Cr \n    Account");
		gd=new GridData();
		//gd.widthHint=95;
		lblCrAccount.setLayoutData(gd);
		
		lblAmount = new Label(innerComp,SWT.BORDER);
		lblAmount.setText("    Amount \n");
		gd=new GridData();
		//gd.widthHint=80;
		lblAmount.setLayoutData(gd);
		
		lblNarrationHeader = new Label(innerComp,SWT.BORDER);
		lblNarrationHeader.setText("                   Narration                       ");
		gd=new GridData();
		//gd.widthHint=130;
		lblNarrationHeader.setLayoutData(gd);
		
		lblProjectName=new Label(innerComp, SWT.BORDER);
		if(projectlist.length > 0)
		{
			
			lblProjectName.setText("     Project \n    Name");
			gd=new GridData();
			//gd.widthHint=100;
			lblProjectName.setLayoutData(gd);
		}
		else
		{
			lblProjectName.setVisible(false);
		}
		
		
		lblaction = new Label(innerComp, SWT.BORDER);
		lblaction.setText("Action \n");
		gd=new GridData();
		//gd.widthHint=50;
		lblaction.setLayoutData(gd);
		
		lblMarkForDel = new Label(innerComp, SWT.BORDER);
		lblMarkForDel.setText("Mark for \n Deletion");
		gd=new GridData();
		//gd.widthHint=73;
		lblMarkForDel.setLayoutData(gd);
		
		scVoucherResult.setContent(innerComp);
	    scVoucherResult.setExpandVertical(true);
	    scVoucherResult.setExpandHorizontal(true);
	    
		btndelete = new Button(this,SWT.PUSH);
		btndelete.setText("&Delete");
		btndelete.setToolTipText("Select the vouchers you wish to delete by checking the checkboxes ");
		btndelete.setFont(new Font(display, "Time New Roman",13,SWT.NORMAL));
		layout= new FormData();
		layout.top = new FormAttachment(90);
		layout.left = new FormAttachment(88);
		layout.right = new FormAttachment(98);
		//layout.bottom = new FormAttachment(96);
		btndelete.setLayoutData(layout);
		btndelete.setVisible(false);
		grpVoucherResult.pack();
				
		this.pack();
		this.getAccessible();
	    this.makeaccssible(this);
	    this.makeaccssible(grpVoucherResult);
	    this.setEvents();
	}
	private void setEvents()
	{
		
		this.combosearchRec.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{
				btnsearch.setEnabled(false);
				String selectedSearchItem = combosearchRec.getItem(combosearchRec.getSelectionIndex());
				
				if(selectedSearchItem.equals("Voucher No"))
				{
					lblEntVoucherNo.setVisible(true);
					txtEntVoucherNo.setVisible(true);
					lblFromDate.setVisible(false);
					txtFromddate.setVisible(false);
					txtFromddate.setText("");
					lblFromDtDash1.setVisible(false);
					txtFromMdate.setVisible(false);
					txtFromMdate.setText("");
					lblFromDtDash2.setVisible(false);
					txtFromYrdate.setVisible(false);
					txtFromYrdate.setText("");
					lblToDate.setVisible(false);
					txtToDdate.setVisible(false);
					txtToDdate.setText("");
					lblToDateDash1.setVisible(false);
					txtToMdate.setVisible(false);
					txtToMdate.setText("");
					lblToDateDash2.setVisible(false);
					txtToYrdate.setVisible(false);
					txtToYrdate.setText("");
					lblentamount.setVisible(false);
					txtentamount.setVisible(false);
					txtentamount.setText("");
					lblNarration.setVisible(false);
					txtnarration.setVisible(false);
					txtnarration.setText("");
					btnsearch.setEnabled(true);
					btnsearch.setVisible(true);
				}
				if(selectedSearchItem.equals("           ---- Please Select -----        "))
				{
					lblEntVoucherNo.setVisible(false);
					txtEntVoucherNo.setVisible(false);
					btnsearch.setEnabled(false);
				}
				
				if(selectedSearchItem.equals("Time Interval (From-To)"))
				{
					lblFromDate.setVisible(true);
					txtFromddate.setVisible(true);
					lblFromDtDash1.setVisible(true);
					txtFromMdate.setVisible(true);
					lblFromDtDash2.setVisible(true);
					txtFromYrdate.setVisible(true);
					lblToDate.setVisible(true);
					txtToDdate.setVisible(true);
					lblToDateDash1.setVisible(true);
					txtToMdate.setVisible(true);
					lblToDateDash2.setVisible(true);
					txtToYrdate.setVisible(true);
					lblEntVoucherNo.setVisible(false);
					txtEntVoucherNo.setVisible(false);
					lblentamount.setVisible(false);
					txtentamount.setVisible(false);
					lblNarration.setVisible(false);
					txtnarration.setVisible(false);
					btnsearch.setEnabled(true);
					btnsearch.setVisible(true);
				}
				if(selectedSearchItem.equals("Amount"))
				{
					
					lblentamount.setVisible(true);
					txtentamount.setVisible(true);
					lblEntVoucherNo.setVisible(false);
					txtEntVoucherNo.setVisible(false);
					lblFromDate.setVisible(false);
					txtFromddate.setVisible(false);
					txtFromddate.setText("");
					lblFromDtDash1.setVisible(false);
					txtFromMdate.setVisible(false);
					txtFromMdate.setText("");
					lblFromDtDash2.setVisible(false);
					txtFromYrdate.setVisible(false);
					txtFromYrdate.setText("");
					lblToDate.setVisible(false);
					txtToDdate.setVisible(false);
					txtToDdate.setText("");
					lblToDateDash1.setVisible(false);
					txtToMdate.setVisible(false);
					txtToMdate.setText("");
					lblToDateDash2.setVisible(false);
					txtToYrdate.setVisible(false);
					txtToYrdate.setText("");
					lblNarration.setVisible(false);
					txtnarration.setVisible(false);
					btnsearch.setEnabled(true);
					btnsearch.setVisible(true);
				}
				
				if(selectedSearchItem.equals("Narration"))
				{
					lblNarration.setVisible(true);
					txtnarration.setVisible(true);
					lblFromDate.setVisible(false);
					txtFromddate.setVisible(false);
					txtFromddate.setText("");
					lblFromDtDash1.setVisible(false);
					txtFromMdate.setVisible(false);
					txtFromMdate.setText("");
					lblFromDtDash2.setVisible(false);
					txtFromYrdate.setVisible(false);
					txtFromYrdate.setText("");
					lblToDate.setVisible(false);
					txtToDdate.setVisible(false);
					txtToDdate.setText("");
					lblToDateDash1.setVisible(false);
					txtToMdate.setVisible(false);
					txtToMdate.setText("");
					lblToDateDash2.setVisible(false);
					txtToYrdate.setVisible(false);
					txtToYrdate.setText("");
					lblentamount.setVisible(false);
					txtentamount.setVisible(false);
					lblEntVoucherNo.setVisible(false);
					txtEntVoucherNo.setVisible(false);
					btnsearch.setEnabled(true);
					btnsearch.setVisible(true);
				}
				
			}
			
		});
		this.btnsearch.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{	
				
				int searchFlag = 0;
				if(combosearchRec.getItem(combosearchRec.getSelectionIndex()).equals("Voucher No"))
				{
					searchFlag = 1;
					btnsearch.setEnabled(true);
					
					String fromDate = globals.session[2].toString().substring(6) + "-" + globals.session[2].toString().substring(3,5) + "-" + globals.session[2].toString().substring(0,2);
					String toDate = globals.session[3].toString().substring(6) + "-" + globals.session[3].toString().substring(3,5) + "-" + globals.session[3].toString().substring(0,2);
					NumberFormat nf = NumberFormat.getInstance();
					nf.setMaximumFractionDigits(2);
					nf.setMinimumFractionDigits(2);
					nf.setGroupingUsed(false);
					Double amount = 0.00;
					try {
							amount = Double.valueOf(nf.format(Double.valueOf(txtentamount.getText())));
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.getMessage();
					}
					Object[] vouchers = gnukhata.controllers.transactionController.searchVouchers(searchFlag, txtEntVoucherNo.getText(), fromDate, toDate, txtnarration.getText(),amount);
					for(int vouchercounter = 0;vouchercounter < vouchers.length; vouchercounter ++)
					{
					Object[] voucherRow = (Object[]) vouchers[vouchercounter]; 
					addRow(voucherRow);
					
					}
					 if(txtEntVoucherNo.getText().trim().equals("") )
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
							msgDayErr.setMessage("Please enter a valid voucherno.");
							msgDayErr.open();
							txtEntVoucherNo.setFocus();
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtEntVoucherNo.setFocus();						
								}
							});
							return;
					}
					 if(vouchers.length == 0)
						{
						
							MessageBox msg = new MessageBox(new Shell(),SWT.OK );
							msg.setMessage("There are no vouchers of this voucher number");
							msg.open();
							
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									
									txtEntVoucherNo.setVisible(true);
									txtEntVoucherNo.setText("");
									txtEntVoucherNo.setFocus();
									
									
								}
								
							});
							return;
						
							//btnsearch.setEnabled(true);
							
						}
					
					grpVoucherResult.pack();
					grpVoucherResult.setVisible(true);
					btndelete.setVisible(true);
					//btnsearch.notifyListeners(SWT.Selection, new Event());
					//btnsearch.setFocus();
					grpVoucherResult.setFocus();
					
					
					
						
					
				}
				if(combosearchRec.getItem(combosearchRec.getSelectionIndex()).equals("Time Interval (From-To)"))
				{
					searchFlag = 2;
					String fromDate = txtFromYrdate.getText() + "-" + txtFromMdate.getText() + "-" + txtFromddate.getText(); 
					String toDate = txtToYrdate.getText() + "-" + txtToMdate.getText() + "-" + txtToDdate.getText(); 
					NumberFormat nf = NumberFormat.getInstance();
					nf.setMaximumFractionDigits(2);
					nf.setMinimumFractionDigits(2);
					nf.setGroupingUsed(false);
					Double amount = 0.00;
					try {
							amount = Double.valueOf(nf.format(Double.valueOf(txtentamount.getText())));
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.getMessage();
					}
					Object[] vouchers = gnukhata.controllers.transactionController.searchVouchers(searchFlag, "", fromDate, toDate, txtnarration.getText(),amount);
					for(int vouchercounter = 0;vouchercounter < vouchers.length; vouchercounter ++)
					{
					Object[] voucherRow = (Object[]) vouchers[vouchercounter]; 
					addRow(voucherRow);
					
					}
					grpVoucherResult.pack();
					
					//btndelete.setVisible(true);
					//btnsearch.notifyListeners(SWT.Selection, new Event());
					//btnsearch.setFocus();
					
					if(txtFromddate.getText().trim().equals("")&&txtFromMdate.getText().trim().equals("")&&txtFromYrdate.getText().trim().equals("")&&txtToDdate.getText().trim().equals("")&&txtToMdate.getText().trim().equals("")&&txtToYrdate.getText().trim().equals("")||txtFromddate.getText().trim().equals(""))
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
						msgDayErr.setMessage("Please enter a valid Date.");
						msgDayErr.open();
						txtFromddate.setFocus();
						
						return;
					}
					
					if(txtFromMdate.getText().trim().equals(""))
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
						msgDayErr.setMessage("Please enter a valid Date.");
						msgDayErr.open();
						txtFromMdate.setFocus();
						
						return;
					}
					
					
					if(txtFromYrdate.getText().trim().equals(""))
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
						msgDayErr.setMessage("Please enter a valid Date.");
						msgDayErr.open();
						txtFromYrdate.setFocus();
						
						return;
					}
					
					if(txtToDdate.getText().trim().equals("")&&txtToMdate.getText().trim().equals("")&&txtToYrdate.getText().trim().equals("")||txtToDdate.getText().trim().equals(""))
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
						msgDayErr.setMessage("Please enter a valid Date.");
						msgDayErr.open();
						txtToDdate.setFocus();
						
						return;
					}
					
					if(txtToMdate.getText().trim().equals(""))
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
						msgDayErr.setMessage("Please enter a valid Date.");
						msgDayErr.open();
						txtToMdate.setFocus();
						
						return;
					}
					if(txtToYrdate.getText().trim().equals(""))
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
						msgDayErr.setMessage("Please enter a valid Date.");
						msgDayErr.open();
						txtToYrdate.setFocus();
						
						return;
					}
					
					
					
					
					if(vouchers.length == 0)
					{
						MessageBox msg = new MessageBox(new Shell(),SWT.OK );
						msg.setMessage("There are no vouchers within this date");
						msg.open();
						
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								
								
								txtFromddate.setFocus();
								
							}
						});
						
						return;
						
					}
					grpVoucherResult.pack();
					grpVoucherResult.setVisible(true);
					btndelete.setVisible(true);
					//btnsearch.notifyListeners(SWT.Selection, new Event());
					//btnsearch.setFocus();
					grpVoucherResult.setFocus();
				
				}
				if(combosearchRec.getItem(combosearchRec.getSelectionIndex()).equals("Narration"))
				{
					searchFlag = 3;
					String fromDate = globals.session[2].toString().substring(6) + "-" + globals.session[2].toString().substring(3,5) + "-" + globals.session[2].toString().substring(0,2);
					String toDate = globals.session[3].toString().substring(6) + "-" + globals.session[3].toString().substring(3,5) + "-" + globals.session[3].toString().substring(0,2);
					NumberFormat nf = NumberFormat.getInstance();
					nf.setMaximumFractionDigits(2);
					nf.setMinimumFractionDigits(2);
					nf.setGroupingUsed(false);
					Double amount = 0.00;
					try {
							amount = Double.valueOf(nf.format(Double.valueOf(txtentamount.getText())));
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.getMessage();
					}
					Object[] vouchers = gnukhata.controllers.transactionController.searchVouchers(searchFlag, "", fromDate, toDate, txtnarration.getText(),amount);
					for(int vouchercounter = 0;vouchercounter < vouchers.length; vouchercounter ++)
					{
					Object[] voucherRow = (Object[]) vouchers[vouchercounter]; 
					addRow(voucherRow);
					grpVoucherResult.pack();
					}
					if(vouchers.length == 0)
					{
						MessageBox msg = new MessageBox(new Shell(),SWT.OK );
						msg.setMessage("There are no vouchers with this narration");
						msg.open();
						
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								
								txtnarration.setVisible(true);
								txtnarration.setFocus();
								
							}
						});
						
						return;
					}
					grpVoucherResult.setVisible(true);
					btndelete.setVisible(true);
					//btnsearch.notifyListeners(SWT.Selection, new Event());
					//btnsearch.setFocus();
					grpVoucherResult.setFocus();
					
					
					
				
					
				} 
			
				if(combosearchRec.getItem(combosearchRec.getSelectionIndex()).equals("Amount"))
				{
					searchFlag = 4;
					String fromDate = globals.session[2].toString().substring(6) + "-" + globals.session[2].toString().substring(3,5) + "-" + globals.session[2].toString().substring(0,2);
					String toDate = globals.session[3].toString().substring(6) + "-" + globals.session[3].toString().substring(3,5) + "-" + globals.session[3].toString().substring(0,2);
					NumberFormat nf = NumberFormat.getInstance();
					nf.setMaximumFractionDigits(2);
					nf.setMinimumFractionDigits(2);
					nf.setGroupingUsed(false);
					Double amount = 0.00;
					try {
							amount = Double.valueOf(nf.format(Double.valueOf(txtentamount.getText())));
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.getMessage();
					}
					Object[] vouchers = gnukhata.controllers.transactionController.searchVouchers(searchFlag, "", fromDate, toDate, "",amount);
					for(int vouchercounter = 0;vouchercounter < vouchers.length; vouchercounter ++)
					{
					Object[] voucherRow = (Object[]) vouchers[vouchercounter]; 
					addRow(voucherRow);
					
					}
					
					if(txtentamount.getText().trim().equals("")||!txtentamount.getText().equals("") && Double.valueOf(txtentamount.getText()) == 0)
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
						msgDayErr.setMessage("Please enter a valid amount.");
						msgDayErr.open();
						txtentamount.setFocus();
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtentamount.setFocus();						
							}
						});
						return;
					}
					/*try {
						txtentamount.setText(nf.format(Double.parseDouble(txtentamount.getText())));
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}*/
					
					if(vouchers.length == 0)
					{
						MessageBox msg = new MessageBox(new Shell(),SWT.OK );
						msg.setMessage("There are no vouchers of given amount");
						msg.open();
						
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								
								txtentamount.setVisible(true);
								txtentamount.setText("");
								txtentamount.setFocus();
								
							}
						});
						
						return;
					}
				
					grpVoucherResult.pack();
					grpVoucherResult.setVisible(true);
					btndelete.setVisible(true);
					//btnsearch.notifyListeners(SWT.Selection, new Event());
					//btnsearch.setFocus();
					grpVoucherResult.setFocus();
					
				}
				
				btnsearch.setEnabled(false);	
					
			}
		});
		
		btndelete.addSelectionListener(new SelectionAdapter() {
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			//super.widgetSelected(arg0);
			//call the deleteVoucher from the transactionController.
			int voucherCounter = 0;
			while( voucherCounter   < viewButton.size() )
			{
				if(ChkDelButton.get(voucherCounter).getSelection())
				{
					transactionController.deleteVoucher(Integer.parseInt(ChkDelButton.get(voucherCounter).getData("vouchercode").toString()));
					voucherNo.get(voucherCounter).dispose();
					voucherNo.remove(voucherCounter);
					voucherType.get(voucherCounter).dispose();
					voucherType.remove(voucherCounter);
					voucherDate.get(voucherCounter).dispose();
					voucherDate.remove(voucherCounter);
					voucherDrAccount.get(voucherCounter).dispose();
					voucherDrAccount.remove(voucherCounter);
					voucherCrAccount.get(voucherCounter).dispose();
					voucherCrAccount.remove(voucherCounter);
					voucherAmount.get(voucherCounter).dispose();
					voucherAmount.remove(voucherCounter);
					voucherProjectName.get(voucherCounter).dispose();
					voucherProjectName.remove(voucherCounter);
					voucherNarration.get(voucherCounter).dispose();
					voucherNarration.remove(voucherCounter);
					viewButton.get(voucherCounter).dispose();
					viewButton.remove(voucherCounter);
					ChkDelButton.get(voucherCounter).dispose();
					ChkDelButton.remove(voucherCounter);
					grpVoucherResult.pack();
				}
				else
				{
					voucherCounter ++;
				}
				//combosearchRec.setFocus();
			}
		}
	});	
		
	btndelete.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) 
		{
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if(arg0.keyCode==SWT.ARROW_UP )
			{
					if(viewButton.size() > 0)
					{
						viewButton.get(counter).setFocus();
					}
					if(viewButton.size()==0)
					{
						combosearchRec.setFocus();
					}
			}
		}
	});
		
		combosearchRec.addFocusListener(new FocusAdapter() {
			 public void focusGained(FocusEvent arg0)
			 {
				 grpVoucherResult.setVisible(false);
					btndelete.setVisible(false); 
			 }
			
		
			});
		
	combosearchRec.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) 
		{
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if(arg0.keyCode==SWT.CR | arg0.keyCode==SWT.KEYPAD_CR)
			{
				if(combosearchRec.getSelectionIndex()==0)
				{
					MessageBox msg = new MessageBox(new Shell(), SWT.ERROR| SWT.OK);
					msg.setMessage("Please Select Valid Option");
					msg.open();
					combosearchRec.setFocus();
				}
				if(combosearchRec.getSelectionIndex()==1)
				{
					txtEntVoucherNo.setFocus();
				}
				if(combosearchRec.getSelectionIndex()==2)
				{
					txtFromddate.setFocus();	
						
				}
				if(combosearchRec.getSelectionIndex()==3)
				{
					txtentamount.setFocus();
				}
				if(combosearchRec.getSelectionIndex()==4)
				{
					txtnarration.setFocus();
				}
			}
		
		}
	});
	txtEntVoucherNo.addFocusListener(new FocusAdapter() {
	 public void focusGained(FocusEvent arg0)
	 {
		 grpVoucherResult.setVisible(false);
			btndelete.setVisible(false); 
			btnsearch.setEnabled(true);
			btnsearch.setVisible(true);
			if(counter==0)
			{
				while(! voucherNo.isEmpty())
				{
					voucherNo.get(0).dispose();
					voucherNo.remove(0);
					voucherType.get(0).dispose();
					voucherType.remove(0);
					voucherDate.get(0).dispose();
					voucherDate.remove(0);
					voucherDrAccount.get(0).dispose();
					voucherDrAccount.remove(0);
					voucherCrAccount.get(0).dispose();
					voucherCrAccount.remove(0);
					voucherAmount.get(0).dispose();
					voucherAmount.remove(0);
					voucherProjectName.get(0).dispose();
					voucherProjectName.remove(0);
					voucherNarration.get(0).dispose();
					voucherNarration.remove(0);
					viewButton.get(0).dispose();
					viewButton.remove(0);
					ChkDelButton.get(0).dispose();
					ChkDelButton.remove(0);
				}

				grpVoucherResult.pack();
				return;
			}
	 }
	
	});
	
	txtEntVoucherNo.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if(arg0.keyCode==SWT.CR | arg0.keyCode==SWT.KEYPAD_CR)
			{
				/*grpVoucherResult.setVisible(true);
				btndelete.setVisible(true);	
				
				grpVoucherResult.setFocus();*/
				
					btnsearch.setFocus();
			
				
			
		
			}
			if(arg0.keyCode==SWT.ARROW_UP)
			{			
				txtEntVoucherNo.setText("");
				combosearchRec.setFocus();
			}
			}
	});
	txtentamount.addVerifyListener(new VerifyListener() {
		
		@Override
		public void verifyText(VerifyEvent arg0) {
			// TODO Auto-generated method stub
			if(verifyFlag== false)
			{
				arg0.doit= true;
				return;
			}
			switch (arg0.keyCode) {
            case SWT.BS:           // Backspace
            case SWT.DEL:          // Delete
            case SWT.HOME:         // Home
            case SWT.END:          // End
            case SWT.ARROW_LEFT:   // Left arrow
            case SWT.ARROW_RIGHT:  // Right arrow
            case SWT.TAB:
            case SWT.CR:
            case SWT.KEYPAD_CR:
                return;
        }
			if(arg0.keyCode==46)
			{
				return;
			}
        if (!Character.isDigit(arg0.character)) {
            arg0.doit = false;  // disallow the action
        }

		}
	});
//first
		txtFromddate.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			
			
			if(arg0.keyCode==SWT.CR | arg0.keyCode==SWT.KEYPAD_CR)
			{
				
					txtFromMdate.setFocus();
					
					
					
				
			}
			if(arg0.keyCode==SWT.ARROW_UP)
			{	
				combosearchRec.setFocus();
			}
		}
	});
	
	txtFromMdate.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if(txtFromMdate.getText().length()==txtFromMdate.getTextLimit())
			{
				txtFromMdate.setFocus();
			}
			
			if(arg0.keyCode==SWT.CR | arg0.keyCode==SWT.KEYPAD_CR)
			{
				txtFromYrdate.setFocus();
			}
			if(arg0.keyCode==SWT.ARROW_UP)
			{	
				txtFromddate.setFocus();
			}
		}
	});
	
	txtFromYrdate.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if(txtFromYrdate.getText().length()==txtFromYrdate.getTextLimit())
			{
				txtFromYrdate.setFocus();
			}
			
			if(arg0.keyCode==SWT.CR | arg0.keyCode==SWT.KEYPAD_CR)
			{
				
					txtToDdate.setFocus();
				
				
			}
			
			if(arg0.keyCode==SWT.ARROW_UP)
			{
				txtFromMdate.setFocus();
			}
		}
	});
	
	txtToDdate.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			
			if(txtToDdate.getText().length()==txtToDdate.getTextLimit())
			{
				txtToDdate.setFocus();
			}
			if(arg0.keyCode==SWT.CR |arg0.keyCode==SWT.KEYPAD_CR)
			{
				txtToMdate.setFocus();
			}
			if(arg0.keyCode==SWT.ARROW_UP)
			{
				txtFromYrdate.setFocus();
			}
		}
	});
	
	txtToMdate.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if(txtToMdate.getText().length()==txtToMdate.getTextLimit())
			{
				txtToMdate.setFocus();
			}
			if(arg0.keyCode==SWT.CR |arg0.keyCode==SWT.KEYPAD_CR)
			{
				txtToYrdate.setFocus();
			}
			if(arg0.keyCode==SWT.ARROW_UP)
			{
				txtToDdate.setFocus();
			}
		}
	});
	
	txtToDdate.addVerifyListener(new VerifyListener() {
		
		@Override
		public void verifyText(VerifyEvent arg0) {
			// TODO Auto-generated method stub
			
			switch (arg0.keyCode) {
            case SWT.BS:           // Backspace
            case SWT.DEL:          // Delete
            case SWT.HOME:         // Home
            case SWT.END:          // End
            case SWT.ARROW_LEFT:   // Left arrow
            case SWT.ARROW_RIGHT:  // Right arrow
            case SWT.TAB:
            case SWT.CR:
            case SWT.KEYPAD_CR:
            case SWT.KEYPAD_DECIMAL:
                return;
        }
			if(arg0.keyCode==46)
			{
				return;
			}
        if (!Character.isDigit(arg0.character)) {
            arg0.doit = false;  // disallow the action
        }

		}
	});
	
	txtToMdate.addVerifyListener(new VerifyListener() {
		
		@Override
		public void verifyText(VerifyEvent arg0) {
			// TODO Auto-generated method stub
			
			switch (arg0.keyCode) {
            case SWT.BS:           // Backspace
            case SWT.DEL:          // Delete
            case SWT.HOME:         // Home
            case SWT.END:          // End
            case SWT.ARROW_LEFT:   // Left arrow
            case SWT.ARROW_RIGHT:  // Right arrow
            case SWT.TAB:
            case SWT.CR:
            case SWT.KEYPAD_CR:
            case SWT.KEYPAD_DECIMAL:
                return;
        }
			if(arg0.keyCode==46)
			{
				return;
			}
        if (!Character.isDigit(arg0.character)) {
            arg0.doit = false;  // disallow the action
        }

		}
	});
	
	txtToYrdate.addVerifyListener(new VerifyListener() {
		
		@Override
		public void verifyText(VerifyEvent arg0) {
			// TODO Auto-generated method stub
			
			switch (arg0.keyCode) {
            case SWT.BS:           // Backspace
            case SWT.DEL:          // Delete
            case SWT.HOME:         // Home
            case SWT.END:          // End
            case SWT.ARROW_LEFT:   // Left arrow
            case SWT.ARROW_RIGHT:  // Right arrow
            case SWT.TAB:
            case SWT.CR:
            case SWT.KEYPAD_CR:
            case SWT.KEYPAD_DECIMAL:
                return;
        }
			if(arg0.keyCode==46)
			{
				return;
			}
        if (!Character.isDigit(arg0.character)) {
            arg0.doit = false;  // disallow the action
        }

		}
	});
	
	txtFromddate.addVerifyListener(new VerifyListener() {
		
		@Override
		public void verifyText(VerifyEvent arg0) {
			// TODO Auto-generated method stub
			if(verifyFlag== false)
			{
				arg0.doit= true;
				return;
			}
			switch (arg0.keyCode) {
            case SWT.BS:           // Backspace
            case SWT.DEL:          // Delete
            case SWT.HOME:         // Home
            case SWT.END:          // End
            case SWT.ARROW_LEFT:   // Left arrow
            case SWT.ARROW_RIGHT:  // Right arrow
            case SWT.TAB:
            case SWT.CR:
            case SWT.KEYPAD_CR:
            case SWT.KEYPAD_DECIMAL:
                return;
        }
			if(arg0.keyCode==46)
			{
				return;
			}
        if (!Character.isDigit(arg0.character)) {
            arg0.doit = false;  // disallow the action
        }

		}
	});
	
txtFromMdate.addVerifyListener(new VerifyListener() {
		
		@Override
		public void verifyText(VerifyEvent arg0) {
			// TODO Auto-generated method stub
			if(verifyFlag== false)
			{
				arg0.doit= true;
				return;
			}
			switch (arg0.keyCode) {
            case SWT.BS:           // Backspace
            case SWT.DEL:          // Delete
            case SWT.HOME:         // Home
            case SWT.END:          // End
            case SWT.ARROW_LEFT:   // Left arrow
            case SWT.ARROW_RIGHT:  // Right arrow
            case SWT.TAB:
            case SWT.CR:
            case SWT.KEYPAD_CR:
            case SWT.KEYPAD_DECIMAL:
                return;
        }
			if(arg0.keyCode==46)
			{
				return;
			}
        if (!Character.isDigit(arg0.character)) {
            arg0.doit = false;  // disallow the action
        }

		}
	});
	
	txtFromYrdate.addVerifyListener(new VerifyListener() {
	
	@Override
	public void verifyText(VerifyEvent arg0) {
		// TODO Auto-generated method stub
		if(verifyFlag== false)
		{
			arg0.doit= true;
			return;
		}
		switch (arg0.keyCode) {
        case SWT.BS:           // Backspace
        case SWT.DEL:          // Delete
        case SWT.HOME:         // Home
        case SWT.END:          // End
        case SWT.ARROW_LEFT:   // Left arrow
        case SWT.ARROW_RIGHT:  // Right arrow
        case SWT.TAB:
        case SWT.CR:
        case SWT.KEYPAD_CR:
        case SWT.KEYPAD_DECIMAL:
            return;
    }
		if(arg0.keyCode==46)
		{
			return;
		}
    if (!Character.isDigit(arg0.character)) {
        arg0.doit = false;  // disallow the action
    }

	}
});
	txtToYrdate.addFocusListener(new FocusAdapter() {
		 public void focusGained(FocusEvent arg0)
		 {
			 grpVoucherResult.setVisible(false);
				btndelete.setVisible(false); 
				btnsearch.setEnabled(true);
				btnsearch.setVisible(true);
				if(counter==0)
				{
					while(! voucherNo.isEmpty())
					{
						voucherNo.get(0).dispose();
						voucherNo.remove(0);
						voucherType.get(0).dispose();
						voucherType.remove(0);
						voucherDate.get(0).dispose();
						voucherDate.remove(0);
						voucherDrAccount.get(0).dispose();
						voucherDrAccount.remove(0);
						voucherCrAccount.get(0).dispose();
						voucherCrAccount.remove(0);
						voucherAmount.get(0).dispose();
						voucherAmount.remove(0);
						voucherProjectName.get(0).dispose();
						voucherProjectName.remove(0);
						voucherNarration.get(0).dispose();
						voucherNarration.remove(0);
						viewButton.get(0).dispose();
						viewButton.remove(0);
						ChkDelButton.get(0).dispose();
						ChkDelButton.remove(0);
					}

					grpVoucherResult.pack();
					return;
				}
		 }
		});
	
	txtToYrdate.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if(txtToYrdate.getText().length()==txtToYrdate.getTextLimit())
			{
				txtToYrdate.setFocus();
			}
			if(arg0.keyCode==SWT.TAB)
			{
				if(txtToYrdate.getText().trim().equals(""))
				{
					//txtToYrdate.setFocus();
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtToYrdate.setFocus();					
						}
					});
					return;
						
				}
				if(!txtToYrdate.getText().trim().equals(""))
				{
					btnsearch.setEnabled(true);
					btnsearch.setFocus();
				}
			}
			if(arg0.keyCode==SWT.CR | arg0.keyCode==SWT.KEYPAD_CR)
			{	
				/*grpVoucherResult.setVisible(true);
				btndelete.setVisible(true);
				btnsearch.notifyListeners(SWT.Selection, new Event());
				//btnsearch.setFocus();
				grpVoucherResult.setFocus();*/
				//btnsearch.notifyListeners(SWT.Selection, new Event());
				
					if(txtToYrdate.getText().trim().equals(""))
					{
						txtToYrdate.setFocus();
						
							
					}
					if(!txtToYrdate.getText().trim().equals(""))
					{
						btnsearch.setEnabled(true);
						btnsearch.setFocus();
					}
					
				
				
			}
			if(arg0.keyCode==SWT.ARROW_UP)
			{
				txtToMdate.setFocus();
			}
		}
	});
	txtentamount.addFocusListener(new FocusAdapter() {
		 public void focusGained(FocusEvent arg0)
		 {
			 verifyFlag=true;
			 txtentamount.selectAll();
			 grpVoucherResult.setVisible(false);
			 btnsearch.setEnabled(true);
				btnsearch.setVisible(true);
				btndelete.setVisible(false); 
				if(counter==0)
				{
					while(! voucherNo.isEmpty())
					{
						voucherNo.get(0).dispose();
						voucherNo.remove(0);
						voucherType.get(0).dispose();
						voucherType.remove(0);
						voucherDate.get(0).dispose();
						voucherDate.remove(0);
						voucherDrAccount.get(0).dispose();
						voucherDrAccount.remove(0);
						voucherCrAccount.get(0).dispose();
						voucherCrAccount.remove(0);
						voucherAmount.get(0).dispose();
						voucherAmount.remove(0);
						voucherProjectName.get(0).dispose();
						voucherProjectName.remove(0);
						voucherNarration.get(0).dispose();
						voucherNarration.remove(0);
						viewButton.get(0).dispose();
						viewButton.remove(0);
						ChkDelButton.get(0).dispose();
						ChkDelButton.remove(0);
					}

					grpVoucherResult.pack();
					return;
				}
		 }
		 @Override
		public void focusLost(FocusEvent arg0) {
			// TODO Auto-generated method stub
			super.focusLost(arg0);
			verifyFlag=false;
		}
		});
	txtentamount.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if(arg0.keyCode==SWT.CR | arg0.keyCode==SWT.KEYPAD_CR)
			{
				btnsearch.setFocus();
			}
			if(arg0.keyCode==SWT.ARROW_UP)
			{
				
				combosearchRec.setFocus();
				txtentamount.setText("");
			}
		}
	});
	
	txtnarration.addFocusListener(new FocusAdapter() {
		 public void focusGained(FocusEvent arg0)
		 {
			 grpVoucherResult.setVisible(false);
			 btnsearch.setEnabled(true);
				btnsearch.setVisible(true);
				btndelete.setVisible(false); 
				
				if(counter==0)
				{
					while(! voucherNo.isEmpty())
					{
						voucherNo.get(0).dispose();
						voucherNo.remove(0);
						voucherType.get(0).dispose();
						voucherType.remove(0);
						voucherDate.get(0).dispose();
						voucherDate.remove(0);
						voucherDrAccount.get(0).dispose();
						voucherDrAccount.remove(0);
						voucherCrAccount.get(0).dispose();
						voucherCrAccount.remove(0);
						voucherAmount.get(0).dispose();
						voucherAmount.remove(0);
						voucherProjectName.get(0).dispose();
						voucherProjectName.remove(0);
						voucherNarration.get(0).dispose();
						voucherNarration.remove(0);
						viewButton.get(0).dispose();
						viewButton.remove(0);
						ChkDelButton.get(0).dispose();
						ChkDelButton.remove(0);
					}

					grpVoucherResult.pack();
					return;
				}
		 }
		});
	txtnarration.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if(arg0.keyCode==SWT.CR | arg0.keyCode==SWT.KEYPAD_CR)
			{/*
				grpVoucherResult.setVisible(true);
				btndelete.setVisible(true);
				btnsearch.notifyListeners(SWT.Selection, new Event());*/
				btnsearch.notifyListeners(SWT.Selection, new Event());
				btnsearch.setEnabled(false);
				//grpVoucherResult.setFocus();
				
			}
			if(arg0.keyCode==SWT.ARROW_UP)
			{
				txtnarration.setText("");
				combosearchRec.setFocus();
			}
		}
	});
	
	btnsearch.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if(arg0.keyCode==SWT.CR | arg0.keyCode==SWT.KEYPAD_CR)
			{
				btnsearch.setVisible(false);
				grpVoucherResult.setEnabled(true);
				grpVoucherResult.setFocus();
			}
			if(arg0.keyCode==SWT.ARROW_UP)
			{
				if(txtnarration.isEnabled()==true)
				{
					txtnarration.setFocus();
				}
				if(txtentamount.isEnabled()==true)
				{
					txtentamount.setFocus();
				}
				if(txtToYrdate.isEnabled()==true)
				{
					txtToYrdate.setFocus();
				}
				if(txtEntVoucherNo.isEnabled()==true)
				{
					txtEntVoucherNo.setFocus();
				}
				
			}
		}
	});
	
	txtFromddate.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if( (arg0.keyCode>= 65 && arg0.keyCode <= 90)||(arg0.keyCode>= 97 && arg0.keyCode <= 122) ||(arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
					arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
					arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9)
			{
				arg0.doit = true;
			}
			else
			{
				
				arg0.doit = false;
			}
		}
	});
	txtFromMdate.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if( (arg0.keyCode>= 65 && arg0.keyCode <= 90)||(arg0.keyCode>= 97 && arg0.keyCode <= 122) ||(arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
					arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
					arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9)
			{
				arg0.doit = true;
			}
			else
			{
				
				arg0.doit = false;
			}
		}
	});
	txtFromYrdate.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if( (arg0.keyCode>= 65 && arg0.keyCode <= 90)||(arg0.keyCode>= 97 && arg0.keyCode <= 122) ||(arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
					arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
					arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9)
			{
				arg0.doit = true;
			}
			else
			{
				
				arg0.doit = false;
			}
		}
	});
	
	txtToDdate.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if( (arg0.keyCode>= 65 && arg0.keyCode <= 90)||(arg0.keyCode>= 97 && arg0.keyCode <= 122) ||(arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
					arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
					arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9)
			{
				arg0.doit = true;
			}
			else
			{
				
				arg0.doit = false;
			}
		}
	});
	txtToMdate.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if( (arg0.keyCode>= 65 && arg0.keyCode <= 90)||(arg0.keyCode>= 97 && arg0.keyCode <= 122) ||(arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
					arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
					arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9)
			{
				arg0.doit = true;
			}
			else
			{
				
				arg0.doit = false;
			}
		}
	});
	txtToYrdate.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if( (arg0.keyCode>= 65 && arg0.keyCode <= 90)||(arg0.keyCode>= 97 && arg0.keyCode <= 122) ||(arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
					arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
					arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9)
			{
				arg0.doit = true;
			}
			else
			{
				
				arg0.doit = false;
			}
		}
	});
	
	txtFromddate.addFocusListener(new FocusAdapter() {
		@Override
		public void focusLost(FocusEvent arg0) {
			// TODO Auto-generated method stub
			//super.focusLost(arg0);
			verifyFlag=false;
			if(!txtFromddate.getText().equals("") && Integer.valueOf ( txtFromddate.getText())<10 && txtFromddate.getText().length()< txtFromddate.getTextLimit())
			{
				txtFromddate.setText("0"+ txtFromddate.getText());
				return;
			}
			if(!txtFromddate.getText().equals("") && (Integer.valueOf(txtFromddate.getText())> 31 || Integer.valueOf(txtFromddate.getText()) <= 0) )
			{
				MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR);
				msgdateErr.setMessage("You have entered an invalid date");
				msgdateErr.open();
				
				txtFromddate.setText("");
				Display.getCurrent().asyncExec(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						txtFromddate.setFocus();							
					}
				});
				return;
			}
			
			
		}
		@Override
		public void focusGained(FocusEvent arg0) {
			// TODO Auto-generated method stub
			super.focusGained(arg0);
			grpVoucherResult.setVisible(false);
			btndelete.setVisible(false); 
			btnsearch.setEnabled(true);
			btnsearch.setVisible(true);
			verifyFlag=true;
		}
	});
	
	
	txtFromYrdate.addFocusListener(new FocusAdapter() {
		@Override
		public void focusLost(FocusEvent arg0) {
			// TODO Auto-generated method stub
			//super.focusLost(arg0);
			verifyFlag=false;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			try {
				Date voucherDate = sdf.parse(txtFromYrdate.getText() + "-" + txtFromMdate.getText() + "-" + txtFromddate.getText());
				Date fromDate = sdf.parse(globals.session[2].toString().substring(6)+ "-" + globals.session[2].toString().substring(3,5) + "-"+ globals.session[2].toString().substring(0,2));
				Date toDate = sdf.parse(globals.session[3].toString().substring(6)+ "-" + globals.session[3].toString().substring(3,5) + "-"+ globals.session[3].toString().substring(0,2));
				
				if(voucherDate.compareTo(fromDate)< 0 || voucherDate.compareTo(toDate) > 0 )
				{
					MessageBox errMsg = new MessageBox(new Shell(),SWT.ERROR |SWT.OK );
					errMsg.setMessage("Please enter the date within the financial year");
					errMsg.open();
					txtFromYrdate.setText("");
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							
							txtFromYrdate.setFocus();
							
						}
					});
					
					return;
				}
			} 
			catch (java.text.ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		@Override
		public void focusGained(FocusEvent arg0) {
			// TODO Auto-generated method stub
			super.focusGained(arg0);
			verifyFlag=true;
			grpVoucherResult.setVisible(false);
			btndelete.setVisible(false); 
			btnsearch.setVisible(true);
			btnsearch.setEnabled(true);
		}
	});
	
	txtFromMdate.addFocusListener(new FocusAdapter() {
		@Override
		public void focusLost(FocusEvent arg0) {
			
			// TODO Auto-generated method stub
			//super.focusLost(arg0);
			verifyFlag=false;
			if(! txtFromMdate.getText().equals("") && Integer.valueOf ( txtFromMdate.getText())<10 && txtFromMdate.getText().length()< txtFromMdate.getTextLimit())
			{
				txtFromMdate.setText("0"+ txtFromMdate.getText());
				return;
			}
			if(!txtFromMdate.getText().equals("") && (Integer.valueOf(txtFromMdate.getText())> 12 || Integer.valueOf(txtFromMdate.getText()) <= 0))
			{
				MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR);
				msgdateErr.setMessage("You have entered an invalid month, please enter it in MM format.");
				msgdateErr.open();
				
				txtFromMdate.setText("");
				Display.getCurrent().asyncExec(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						
						txtFromMdate.setFocus();
						
					}
				});
				return;
				
			}
			
			
		}
		@Override
		public void focusGained(FocusEvent arg0) {
			// TODO Auto-generated method stub
			super.focusGained(arg0);
			verifyFlag=true;
			grpVoucherResult.setVisible(false);
			btndelete.setVisible(false); 
			btnsearch.setEnabled(true);
			btnsearch.setVisible(true);
		}
		
	});
	
		txtToDdate.addFocusListener(new FocusAdapter() {
		@Override
		public void focusLost(FocusEvent arg0) {
			// TODO Auto-generated method stub
			//super.focusLost(arg0);
			if(!txtToDdate.getText().equals("") && Integer.valueOf ( txtToDdate.getText())<10 && txtToDdate.getText().length()< txtToDdate.getTextLimit())
			{
				txtToDdate.setText("0"+ txtToDdate.getText());
			}
			if(!txtToDdate.getText().equals("") && (Integer.valueOf(txtToDdate.getText())> 31 || Integer.valueOf(txtToDdate.getText()) <= 0) )
			{
				MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR);
				msgdateErr.setMessage("You have entered an invalid date");
				msgdateErr.open();
				
				txtToDdate.setText("");
				Display.getCurrent().asyncExec(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						txtToDdate.setFocus();
						
					}
				});
				
			}
			
		}
@Override
	public void focusGained(FocusEvent arg0) {
		// TODO Auto-generated method stub
		//super.focusGained(arg0);
		verifyFlag=true;
		btnsearch.setEnabled(true);
		btnsearch.setVisible(true);
		grpVoucherResult.setVisible(false);
		btndelete.setVisible(false); 
	}
	});
	
	txtToMdate.addFocusListener(new FocusAdapter() {
		@Override
		public void focusLost(FocusEvent arg0) {
			// TODO Auto-generated method stub
			//super.focusLost(arg0);
			if(!txtToMdate.getText().equals("") && Integer.valueOf ( txtToMdate.getText())<10 && txtToMdate.getText().length()< txtToMdate.getTextLimit())
			{
				 txtToMdate.setText("0"+  txtToMdate.getText());
			}
			if(!txtToMdate.getText().equals("") && (Integer.valueOf(txtToMdate.getText())> 12 || Integer.valueOf(txtToMdate.getText()) <= 0) )
			{
				MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR);
				msgdateErr.setMessage("You have entered an invalid month");
				msgdateErr.open();
				
				txtToMdate.setText("");
				Display.getCurrent().asyncExec(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						txtToMdate.setFocus();
						
					}
				});
				
			}
			
		}
		@Override
		public void focusGained(FocusEvent arg0) {
			// TODO Auto-generated method stub
			//super.focusGained(arg0);
			verifyFlag=true;
			grpVoucherResult.setVisible(false);
			btndelete.setVisible(false); 
			btnsearch.setEnabled(true);
			btnsearch.setVisible(true);
		}
	});
	
	
	txtToYrdate.addFocusListener(new FocusAdapter() {
		
		@Override
		public void focusLost(FocusEvent arg0) {
			// TODO Auto-generated method stub
			//super.widgetSelected(arg0);
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			try {
				Date ledgerStart = sdf.parse(txtFromYrdate.getText()+ "-"+ txtFromMdate.getText()+"-"+ txtFromddate.getText() );
				Date ledgerEnd = sdf.parse(txtToYrdate.getText()+ "-"+ txtToMdate.getText()+"-"+ txtToDdate.getText() );
				Date financialStart = sdf.parse(globals.session[2].toString().substring(6) +"-"+globals.session[2].toString().substring(3,5)+"-"+ globals.session[2].toString().substring(0,2));
				Date financialEnd = sdf.parse(globals.session[3].toString().substring(6) +"-"+globals.session[3].toString().substring(3,5)+"-"+ globals.session[3].toString().substring(0,2));
				if((ledgerStart.compareTo(financialStart)< 0 || ledgerStart.compareTo(financialEnd)>0)|| (ledgerEnd.compareTo(financialStart)<0 || ledgerEnd.compareTo(financialEnd)> 0 ) )
				{
					MessageBox msg = new MessageBox(new Shell(),SWT.ERROR|SWT.OK );
					msg.setMessage("Please enter the date range within the financial year");
					grpVoucherResult.setVisible(false);
					btndelete.setVisible(false);
					msg.open();
					txtToYrdate.setText("");
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtToYrdate.setFocus();
						}
					});
					
					return;
				}
								
			} catch(java.text.ParseException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(!txtToYrdate.getText().trim().equals(""))
			{
				btnsearch.setEnabled(true);
			}
		}
		@Override
		public void focusGained(FocusEvent arg0) {
			// TODO Auto-generated method stub
			//super.focusGained(arg0);
			verifyFlag=true;
			grpVoucherResult.setVisible(false);
			btndelete.setVisible(false); 
			btnsearch.setEnabled(true);
			btnsearch.setVisible(true);
		}
		
	});
	
	
/*	txtentamount.addFocusListener(new FocusAdapter() {
		@Override
		public void focusLost(FocusEvent arg0) {
			// TODO Auto-generated method stub
			//super.focusLost(arg0);
			if(txtentamount.getText().trim().equals(""))
			{
				MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
				msgDayErr.setMessage("Please enter a valid amount.");
				msgDayErr.open();
				txtentamount.setFocus();
				display.getCurrent().asyncExec(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						txtentamount.setFocus();						
					}
				});
				return;
			}
			
			try {
				txtentamount.setText(nf.format(Double.parseDouble(txtentamount.getText())));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
		}
	
	});*/
	
		
}
	 public void makeaccssible(Control c)
		{
			c.getAccessible();
			
			
		}
	private void addRow(Object[] voucherRow)
	{
		//grpVoucherResult.pack();
		totalWidth = innerComp.getClientArea().width;
		
		Object[] voucherprojectname = transactionController.getVoucherMaster(Integer.valueOf(voucherRow[0].toString() ));
		String projectname=voucherprojectname[4].toString();
		
		projectlist=transactionController.getAllProjects();
		
		
		/*MessageBox msg=new MessageBox(new Shell(),SWT.OK);
		msg.setMessage("Project name"+projectlist.length);
		msg.open();*/
		
		GridData gd=new GridData();
		lblvoucherno=new Label(innerComp, SWT.BORDER);
		lblvoucherno.setText( voucherRow[1].toString());
		gd = new GridData();
		gd.horizontalAlignment = SWT.CENTER;
		gd.widthHint = 3 * totalWidth / 100;
		lblvoucherno.setLayoutData(gd);
		
		lblvoucherdate=new Label(innerComp, SWT.BORDER);
		lblvoucherdate.setText( voucherRow[3].toString());
		gd = new GridData();
		gd.horizontalAlignment = SWT.CENTER;
		gd.widthHint = 8 * totalWidth / 100;
		lblvoucherdate.setLayoutData(gd);
		
		lblvouchertype=new Label(innerComp, SWT.BORDER);
		lblvouchertype.setText( voucherRow[2].toString().trim() );		
		gd = new GridData();
		gd.horizontalAlignment = SWT.LEFT;
		//gd.grabExcessHorizontalSpace = true;
		gd.widthHint = 8 * totalWidth / 100;
		lblvouchertype.setLayoutData(gd);		
		
		lbldraccount=new Label(innerComp, SWT.BORDER);
		lbldraccount.setText( voucherRow[4].toString());
		gd = new GridData();
		gd.horizontalAlignment = SWT.CENTER;
		gd.widthHint = 10 * totalWidth / 100;
		lbldraccount.setLayoutData(gd);
		
		lblcraccount=new Label(innerComp, SWT.BORDER);
		lblcraccount.setText( voucherRow[5].toString());
		gd = new GridData();
		gd.horizontalAlignment = SWT.CENTER;
		gd.widthHint = 10 * totalWidth / 100;
		lblcraccount.setLayoutData(gd);
		
		lblamount=new Label(innerComp, SWT.BORDER|SWT.RIGHT);
		lblamount.setText( voucherRow[6].toString());
		gd = new GridData();
		gd.horizontalAlignment = SWT.CENTER;
		gd.widthHint = 8 * totalWidth / 100;		
		lblamount.setLayoutData(gd);
		
		
				
		lblnarration=new Label(innerComp, SWT.BORDER);
		lblnarration.setText( voucherRow[7].toString());
		gd = new GridData();
		gd.horizontalAlignment = SWT.CENTER;
		gd.widthHint = 20 * totalWidth / 100;
		lblnarration.setLayoutData(gd);	
		
		lblprojectname=new Label(innerComp, SWT.BORDER);
		gd = new GridData();
		gd.horizontalAlignment = SWT.CENTER;
		gd.widthHint = 9 * totalWidth / 100;
		lblprojectname.setLayoutData(gd);
		
		if (projectlist.length > 0) 
		{
			lblprojectname.setText(projectname);
			
		}
		else 
		{
			lblprojectname.setVisible(false);
		}
		
		
		
		
		
		voucherNo.add(lblvoucherno);
		voucherType.add(lblvouchertype);
		voucherDate.add(lblvoucherdate);
		voucherDrAccount.add(lbldraccount);
		voucherCrAccount.add(lblcraccount);
		voucherAmount.add(lblamount);
		voucherNarration.add(lblnarration);
		voucherProjectName.add(lblprojectname);
		
		
				
		Button btnView = new Button(innerComp, SWT.PUSH);
		btnView.setText("View");
		btnView.setData("vouchercode",Integer.valueOf(voucherRow[0].toString() ) );
		gd.horizontalAlignment = SWT.CENTER;
		
		gd=new GridData();
		gd.widthHint = 6 * totalWidth / 100;
		btnView.setLayoutData(gd);
		viewButton.add(btnView);
		
		Button markForDeletion = new Button(innerComp, SWT.CHECK|SWT.CENTER);
		markForDeletion.setData("vouchercode", voucherRow[0]);
		markForDeletion.setVisible(true);
		gd=new GridData();
		gd.horizontalAlignment = SWT.CENTER;
		gd.widthHint = 6 * totalWidth / 100;		
		markForDeletion.setLayoutData(gd);
		ChkDelButton.add(markForDeletion);
		grpVoucherResult.pack();
		scVoucherResult.setContent(innerComp);
	    scVoucherResult.setExpandVertical(true);
	    scVoucherResult.setExpandHorizontal(true);
	    scVoucherResult.setMinSize(innerComp.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	    
		btnView.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				
				Composite grandParent=(Composite) viewButton.get(0).getParent().getParent().getParent().getParent().getParent().getParent().getParent();
				int vouchercode = Integer.valueOf(arg0.widget.getData("vouchercode").toString());
				viewButton.get(0).getParent().getParent().getParent().getParent().getParent().getParent().dispose();
					//transactionController.showVoucherDetail(grandParent, typeFlag, vouchercode,false , "", false, "", "", "");
			
				if(findvoucher==true)
				{
					transactionController.showVoucherDetail(grandParent,typeFlag, vouchercode,findvoucher);
					transactionController.getVoucherMaster(vouchercode);
					
				}
				else
				{
					transactionController.showVoucherDetail(grandParent,typeFlag, vouchercode,false);
					transactionController.getVoucherMaster(vouchercode);
				}
				}
			});
		
		innerComp.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				if(viewButton.get(0).isVisible())
				{
				viewButton.get(0).setFocus();
				}
				else
				{
					if(txtEntVoucherNo.isVisible())
					{
						txtEntVoucherNo.setFocus();
					}
					if(txtFromddate.isVisible())
					{
						txtFromddate.setFocus();
					}
					if(txtentamount.isVisible())
					{
						txtentamount.setFocus();
					}
					if(txtnarration.isVisible())
					{
						txtnarration.setFocus();
					}
				}
			}
		});
		
		btnView.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
			@Override
			public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_DOWN && counter < viewButton.size()-1 )
				{
					counter++;
					if(counter >= 0 && counter < viewButton.size())
					{	
					viewButton.get(counter).setFocus();
					}
				}
				if(arg0.keyCode==SWT.ARROW_UP && counter > 0)
				{
					counter--;
					if(counter >= 0 && counter < viewButton.size())
					{	
					viewButton.get(counter).setFocus();
					}
				}
				if(arg0.keyCode==SWT.ARROW_RIGHT)
				{
					ChkDelButton.get(counter).setFocus();
				}
				if(arg0.keyCode==SWT.ARROW_UP && counter==0)
				{
					combosearchRec.setFocus();
					btnsearch.setVisible(true);
					/*MessageBox msg = new MessageBox(new Shell(), SWT.OK);
					msg.setMessage(Integer.toString(viewButton.size()));
					msg.open();*/
					while(! voucherNo.isEmpty())
					{
						voucherNo.get(0).dispose();
						voucherNo.remove(0);
						voucherType.get(0).dispose();
						voucherType.remove(0);
						voucherDate.get(0).dispose();
						voucherDate.remove(0);
						voucherDrAccount.get(0).dispose();
						voucherDrAccount.remove(0);
						voucherCrAccount.get(0).dispose();
						voucherCrAccount.remove(0);
						voucherAmount.get(0).dispose();
						voucherAmount.remove(0);
						voucherProjectName.get(0).dispose();
						voucherProjectName.remove(0);
						voucherNarration.get(0).dispose();
						voucherNarration.remove(0);
						viewButton.get(0).dispose();
						viewButton.remove(0);
						ChkDelButton.get(0).dispose();
						ChkDelButton.remove(0);
					}

					grpVoucherResult.pack();
					return;
				}
			}
		});
		
		markForDeletion.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
			@Override
			public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_DOWN && counter < ChkDelButton.size()-1 )
				{
					counter++;
					if(counter >= 0 && counter < viewButton.size())
					{	
					ChkDelButton.get(counter).setFocus();
					}
				}
				if(arg0.keyCode==SWT.ARROW_UP && counter > 0)
				{
					counter--;
					if(counter >= 0 && counter < viewButton.size())
					{	
					ChkDelButton.get(counter).setFocus();
					}
				}
				if(arg0.keyCode==SWT.ARROW_LEFT)
				{
					viewButton.get(counter).setFocus();
				}
			}
		});
		
		
		//forView.add(btnView);
			 }
}