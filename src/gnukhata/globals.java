package gnukhata;

import gnukhata.views.startupForm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Vector;


//import org.apache.xmlrpc.XmlRpcClient;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

/**
 * @author 
 * Girish Joshi <girish946@gmail.com>, 
 * Krishnakant Mane <kk@dff.org.in>, 
 * Ujwala Pawade <ujwalahpawade@gmail.com>
 * 
 */
/*
 * this class contains all the global data objects required by the gnukhata's
 * frontend.
 */
public class globals
{
	/*
	 * static array contains data objects 1.client_id 2.Organisation name
	 * 3.starting date of the financial year. 4.end date of the financial year.
	 * 5.organisation type. 6.user role.
	 */

	// TODO make appropriate assignment to the elements
//the global array for storing details including clientid for all transactions.
	//this also contains the orgname,financialstart financialend and orgtype.
	public static Object[] session = new Object[9];
	public static Display display;
	public static Image logo;
	public static Image icon;
	public static Image img;
	public static Image img1;
	static
	{
		try
		{
			logo = new Image(display, startupForm.class.getResourceAsStream("/images/finallogo1.png"));
		} catch (Exception e)
		{
			System.out.println("Logo not found");
			e.printStackTrace();
		}

		try
		{
			icon = new Image(display, startupForm.class.getResourceAsStream("/images/icon.png"));
		} catch (Exception e)
		{
			System.out.println("Icon not found");
			e.printStackTrace();
		}
		
		try
		{
			img = new Image(display, startupForm.class.getResourceAsStream("/images/kelkar_crop.jpeg"));
		} catch (Exception e)
		{
			System.out.println("Image not found");
			e.printStackTrace();
		}
		try
		{
			img1 = new Image(display, startupForm.class.getResourceAsStream("/images/kk_crop.jpg"));
		} catch (Exception e)
		{
			System.out.println("Image not found");
			e.printStackTrace();
		}
	
	//the xmlrpcclient is named client and used to execute the functions from the rpc server.
	//we will use the execute method of this client to execute any function from core_engine.
	try {
		Runtime r = Runtime.getRuntime();
		String[] command = {"sh","-c","echo $DESKTOP_SESSION"};
		Process proc = r.exec(command);

		try {
			proc.waitFor();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		BufferedReader bf = new BufferedReader(new InputStreamReader(proc.getInputStream()));
		String s = null;
		while ((s = bf.readLine()) != null) {
			System.out.println(s);
			session[8] = s;
		}
		
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	}

	public static XmlRpcClient client;
	
	static
	{
		try
		{
			client = new XmlRpcClient();
			XmlRpcClientConfigImpl conf = new XmlRpcClientConfigImpl();
			conf.setServerURL(new URL("http://localhost:7081"));
			conf.setEnabledForExtensions(true);
			client.setConfig(conf);


		} catch (MalformedURLException e)
		{
			e.printStackTrace();
		}
	}
	/*public static void main(String[] arts) {
	

		// Vector<Object> params = new Vector<Object>();
		 Object[] params = new Object[] {};

		try
		{
			Object[] result = (Object[]) globals.client.execute("getOrganisationNames", params);
			//client.call("calculate",5,5,5 );
			System.out.println("success");
			for (int i = 0; i < result.length; i++)
			{
				System.out.println(result[i].toString());

			}
			System.out.println(result.length);
		} 
		catch (Exception e)
		{
			e.printStackTrace();

		}
	}*/
}
